import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { Product } from './products.entity';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
  constructor(
    private productService: ProductsService,
    @Inject('PRODUCT_SERVICE') private readonly client: ClientProxy,
  ) {}

  @Get()
  async getAll() {
    this.client.emit('Hello', 'Hello from RMQ');
    return this.productService.getAll();
  }

  @Get(':id')
  async getById(@Param('id') id: number) {
    return this.productService.getById(id);
  }

  @Post()
  async create(@Body() product: Product) {
    return this.productService.create(product);
  }

  @Patch(':id')
  async update(@Param() id: number, @Body() product: Product) {
    return this.productService.update(id, product);
  }

  @Delete(':id')
  async delete(@Param() id: number) {
    return this.productService.delete(id);
  }
}
