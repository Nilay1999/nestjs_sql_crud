import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './products.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>,
  ) {}

  async getAll() {
    return this.productRepository.find();
  }

  async getById(id: number) {
    return this.productRepository.findOne({ id });
  }

  async create(product: Product) {
    const isProductExists = await this.productRepository.find({
      name: product.name,
    });

    if (isProductExists.length == 0) {
      return this.productRepository.save(product);
    } else {
      return 'Product Already Exists';
    }
  }

  async update(id: number, product: Product) {
    return this.productRepository.update(id, product);
  }

  async delete(id: number) {
    return this.productRepository.delete(id);
  }
}
